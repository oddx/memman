module Main where

import Control.Monad
import Data.List
import System.Directory
import System.Environment
import System.FilePath.Posix
import System.Exit

main :: IO ()
main = do
   xs@(src:target:_) <- getArgs
   exists            <- liftM and (mapM doesPathExist xs)
   validArgs         <- if length xs == 2 && exists
                           then matchContents src target
                           else print "memman srcpath destpath"
   exitSuccess


--get filenames less extentions, gather raw files that match, move to raw directory, move jpgs to target
matchContents src target = do
   -- createDirectoryIfMissing True (target ++ "raw/")
   srcFilenames <- liftM (map takeBaseName) (listDirectory src)
   let srcFiles = filter (\x -> length x > 0) srcFilenames 
   targetFilenames <- listDirectory target
   files <- return (filtermutate srcFiles targetFilenames isInfixOf)
   moveFiles src target files
   
--review this
filtermutate xs ys pf = do
  p <- map pf xs
  filter p ys 

moveFiles src target files = do
   let rawFiles = filter (\x -> takeExtension x /= ".jpg") files
   print rawFiles

-- take two args: source directory, destination directory
-- gather filenames from source
-- check paths exist
-- check for raw directory
-- create raw directory if not already there
-- move to destination
-- move raw files that match the edits to raw directory

-- NOTES
-- parse xs
--       | length xs == 2 = xs
--       | otherwise      = print "memman srcpath destpath"

-- validate xs
--       -- research better check
--       | liftM and (mapM doesPathExist xs) = matchContents xs
--       | otherwise             = print "path doesn't exist"

-- of note: proper lift usage to compare values in monad
-- mComp val f x = liftM (== val) (f x)
-- setExists x = if x == True then print "heyo" else print "nope"
-- all' xs = liftM and (mapM doesPathExist xs)
-- setExists (all' xs)

